package com.wangrui;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.wangrui.domain.User;
import com.wangrui.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Wrapper;
import java.util.Date;
import java.util.List;

@SpringBootTest
class MpOracleApplicationTests {
    @Autowired
    private UserMapper userMapper;

    @Test
    void contextLoads() {
    }

    @Test
    public void testSelect(){
        List<User> users = userMapper.selectList(Wrappers.<User>query().eq("name", "王瑞"));
        System.out.println(users);
    }

    @Test
    public void testInsert(){
        User user = new User();
        user.setAge(212L);
        user.setName("王瑞2");
        user.setBirthday(new Date());
        userMapper.insert(user);
    }
}
