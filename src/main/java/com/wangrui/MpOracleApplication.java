package com.wangrui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MpOracleApplication {

    public static void main(String[] args) {
        SpringApplication.run(MpOracleApplication.class, args);
    }

}
