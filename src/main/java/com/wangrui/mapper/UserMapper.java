package com.wangrui.mapper;

import com.wangrui.domain.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Administrator
* @description 针对表【T_USER】的数据库操作Mapper
* @createDate 2024-01-07 20:09:45
* @Entity com.wangrui.domain.User
*/
public interface UserMapper extends BaseMapper<User> {

}




