package com.wangrui.controller;

import com.wangrui.domain.User;
import com.wangrui.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public class OracleController {
    @Autowired
    private UserMapper userMapper;

    @GetMapping("/insert")
    public String insert(){
        User user = new User();
        user.setAge(23L);
        user.setName("王瑞");
        user.setBirthday(new Date());
        userMapper.insert(user);
        return "success";
    }
}
